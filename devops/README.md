## Requirements
* [Docker](https://www.docker.com/).
* [Docker Compose](https://docs.docker.com/compose/install/).

## Development flow
**Note**: For Windows users, LF must be set to successfully run the containerized stack.
Ensure git is properly configured before cloning this repository by running:

```bash
$ git config --global core.autocrlf false
```

* Include an `.env` file in the `devops/docker` directory. See `devops/docker/compose.env.dev`
serves for reference.

```bash
$ cp compose.env.dev .env
```

* Start the stack with Docker Compose from the `devops/docker` directory:

```bash
$ docker-compose up -d
```

* Now you can open your browser and interact with these URLs:

Backend, JSON based web API based on OpenAPI: http://localhost/backend/

Automatic interactive documentation with Swagger UI (from the OpenAPI backend): http://localhost/docs

Alternative automatic documentation with ReDoc (from the OpenAPI backend): http://localhost/redoc

PGAdmin, PostgreSQL web administration: http://localhost:5050

Traefik UI, to see how the routes are being handled by the proxy: http://localhost:8090

**Note**: It might take a minute for the stack to be ready if it is your initial build.

To check the logs for **ALL** of the services, run:

```bash
$ docker-compose logs
```

To check the logs of a specific service, add the name of the service.
E.g.
```bash
$ docker-compose logs api
```

#### Main `docker-compose.yml`
The main configuration defines:

- core services specs
- variables used in all environments
- networking

#### Development `docker-compose.dev.yml`
Dev configuration inherits and extends the main one and adds:
- image build instructions
- local mounted volumes
- exposed ports to host machine

### Application environment
Custom application environment, feel free to adjust.

```env
APP_ENV=development # development, staging, production
```

### Node environment
Node env enables special code optimizations in production builds. Application environment
such as `staging` normally will benefit of the code optimizations. Better [not to be used](https://glebbahmutov.com/blog/do-not-use-node-env-for-staging/)
as application environment.

```env
NODE_ENV=development # development, production
```

### Multi-stage build step

Target environment defines the end stage of the [multi-stage build](https://docs.docker.com/develop/develop-images/multistage-build/)

You can read more about the different stages in the main [README.md](https://github.com/kachar/yadi)

```env
TARGET_ENV=development # dependencies, development, builder, production
```

### Combinations

| `APP_ENV`   | `NODE_ENV`  | `TARGET_ENV` | Notes                                                   |
| ----------- | ----------- | ------------ | ------------------------------------------------------- |
| development | development | development  | Normal development flow                                 |
| development | production  | production   | Local production build                                  |
| staging     | development | development  | Not very useful, can help reach certain code conditions |
| staging     | production  | production   | Production build deployed on staging server             |
| production  | production  | production   | We're live                                              |

### `.env` files
The `.env` file contains all of your configurations, generated keys and passwords, etc.
Due to its sensitive nature, it **SHOULD NOT** be included in Git. In the `devops/docker/env`
directory, there exists an `.env` file with the appropriate configuartion for
local development for each service. Copy the contents into an `.env` file within
the same directory to get started.

E.g.
```bash
$ cp api.env.dev api.env
```

### Docker Compose
With the `COMPOSE_FILE` we load two linked configurations in development mode.

```env
COMPOSE_FILE=docker-compose.yml:docker-compose.dev.yml
```

Note: On Windows use `;` as file [separator](https://docs.docker.com/compose/reference/envvars/#compose_file)
instead of `:`.

During development, you can change Docker Compose settings that will only affect the local
development environment by editing `devops/docker/docker-compose.dev.yml`.

The changes to that file only affect the local development environment, not the production
environment. So, you can add "temporary" changes that help the development workflow.

For example, the directory with the backend code is mounted as a Docker "host volume",
mapping the code you change live to the directory inside the container. That allows you
to test your changes right away, without having to build the Docker image again. It should
only be done during development, for production, you should build the Docker image with a
recent version of the backend code. But during development, it allows you to iterate very fast.

There is also a command override that runs `/dev-start.sh` (included in the base image)
instead of the default `/start.sh` (also included in the base image). It starts a single
server process (instead of multiple, as would be for production) and reloads the process
whenever the code changes. Have in mind that if you have a syntax error and save the Python
file, it will break and exit, and the container will stop. After that, you can restart the
container by fixing the error and running again:

```console
$ docker-compose up -d
```

There is also a commented out `command` override, you can uncomment it and comment the
default one. It makes the backend container run a process that does "nothing", but keeps
the container alive. That allows you to get inside your running container and execute
commands inside, for example a Python interpreter to test installed dependencies, or
start the development server that reloads when it detects changes.

To get inside the container with a `bash` session you start the stack with:

```console
$ docker-compose up -d
```

and then `exec` inside the running container:

```console
$ docker-compose exec api bash
```

You should see an output like:

```console
root@7f2607af31c3:/app#
```

that means that you are in a `bash` session inside your container, as a `root` user
under the `/app` directory.

There you can use the script `/dev-start.sh` to run the debug live reloading server.
You can run that script from inside the container with:

```console
$ bash /dev-start.sh
```


### Change the development "domain"
If you need to use your local stack with a different domain than `localhost`,
you need to make sure the domain you use points to the IP where your stack is set up.

To simplify your Docker Compose setup, for example, so that the API docs (Swagger UI)
knows where is your API, you should let it know you are using that domain for development.
You will need to edit 1 line in 2 files.

* Open the file located at `devops/docker/env/compose.env`. It should have the line:

```
DOMAIN=localhost
```

* Change it to the domain you are going to use, e.g.:

```
DOMAIN=local.sandbox.com
```

That variable will be used by the Docker Compose files.

* Change that line to the domain you are going to use

After changing the line, you can re-start your stack with:

```bash
docker-compose up -d
```

and check all the corresponding available URLs in the section at the end.

### Persisting Docker named volumes
You need to make sure that each service (Docker container) that uses a volume is always
deployed to the same Docker "node" in the cluster, that way it will preserve the data.
Otherwise, it could be deployed to a different node each time, and each time the volume
would be created in that new node before starting the service. As a result, it would look
like your service was starting from scratch every time, losing all the previous data.

That's specially important for a service running a database. But the same problem would
apply if you were saving files in your main backend service (for example, if those files
were uploaded by your users, or if they were created by your system).

To solve that, you can put constraints in the services that use one or more data volumes
(like databases) to make them be deployed to a Docker node with a specific label. And of
course, you need to have that label assigned to one (only one) of your nodes.
