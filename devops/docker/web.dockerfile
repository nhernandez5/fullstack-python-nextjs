# Build target base #
#####################
FROM node:16.15-buster-slim AS base
WORKDIR /app
ARG NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED 1
ENV PATH=/app/node_modules/.bin:$PATH \
    NODE_ENV="$NODE_ENV"
RUN apt update && apt upgrade && apt-get install -y curl
COPY ./web/package.json /app/
EXPOSE 3000

# Build target dependencies #
#############################
FROM base AS dependencies
# Install prod dependencies
RUN npm install --production && \
    # Cache prod dependencies
    cp -R node_modules /prod_node_modules && \
    # Install dev dependencies
    npm install --production=false

# Build target development #
############################
FROM dependencies AS development
COPY ./web /app
CMD ["yarn", "dev"]

# Build target builder #
########################
FROM base AS builder
COPY --from=dependencies /app/node_modules /app/node_modules
COPY ./web /app
# NEXT does not accept injected environment variables once compiled
# Need to make sure environment variables are set to prod when building
# TODO: Find a better solution
RUN yarn build && \
    rm -rf node_modules

# Build target production #
###########################
FROM base AS production

RUN addgroup --gid 1001 --system nodejs
RUN adduser --system app --uid 1001

COPY --from=builder /app/public /app/public
COPY --from=builder --chown=app:nodejs /app/.next /app/.next
COPY --from=dependencies --chown=app:nodejs /prod_node_modules /app/node_modules

ARG WEB_PORT=80
EXPOSE ${WEB_PORT}
ENV PORT ${WEB_PORT}

CMD ["npm", "start"]

# HEALTHCHECK --interval=5s --timeout=5s --retries=3 \
#     CMD curl --fail http://localhost:3000 || exit 1