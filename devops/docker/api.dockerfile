FROM python:3.10-slim

WORKDIR /api/

# Install Poetry
RUN apt-get update && \
    apt-get install -y curl && \
    apt-get install gcc -y && \
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | POETRY_HOME=/opt/poetry python && \
    cd /usr/local/bin && \
    ln -s /opt/poetry/bin/poetry && \
    poetry config virtualenvs.create false

RUN /usr/local/bin/python3 -m pip install --upgrade pip

# Copy the start scripts over to the root directory to avoid being
# overwritten on volume mounting
COPY ./api/start.sh ./api/dev-start.sh /
COPY ./api /api

# Allow installing dev dependencies to run tests
RUN bash -c "if [ $APP_ENV == 'development' ] ; then poetry install --no-root ; else poetry install --no-root --no-dev ; fi"

RUN chmod +x /start.sh && chmod +x /dev-start.sh
ENV PYTHONPATH=/api

EXPOSE 80

# Run the start script, it will check for the /api/prestart.sh script (e.g. for migrations)
# And then will start Gunicorn with Uvicorn
CMD ["/start.sh"]