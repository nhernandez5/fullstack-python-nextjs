import secrets
from typing import Any, Dict, Optional

from pydantic import BaseSettings, EmailStr, PostgresDsn, validator

from app.core.app_environment import AppEnvironment


class Settings(BaseSettings):
    API_PREFIX: str
    APP_ENV: str

    RUNTIME_ENV: Optional[AppEnvironment] = None

    @validator("RUNTIME_ENV", pre=True)
    def assemble_is_dev(cls, v: str, values: Dict[str, Any]) -> AppEnvironment:
        return AppEnvironment.from_str(values["APP_ENV"].upper())

    SECRET_KEY: str = secrets.token_urlsafe(32)

    # 60 minutes * 24 hours * 30 days = 30 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 15
    REFRESH_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 30

    POSTGRES_SERVER: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    POSTGRES_PORT: Optional[str]
    POSTGREST_SSL_CERT: Optional[str]
    SQLALCHEMY_DATABASE_URI: Optional[str] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v

        return PostgresDsn.build(
            scheme="postgresql+asyncpg",
            user=values.get("POSTGRES_USER"),
            password=values.get("POSTGRES_PASSWORD"),
            host=values.get("POSTGRES_SERVER"),
            path=f"/{values.get('POSTGRES_DB') or ''}",
            port=f"{values.get('POSTGRES_PORT') or '5432'}",
        )

    class Config:
        case_sensitive = True


settings = Settings()
