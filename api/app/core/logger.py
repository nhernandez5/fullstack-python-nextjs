import logging

from app.core.app_environment import AppEnvironment
from app.core.config import settings


logging_level = (
    logging.INFO
    if settings.RUNTIME_ENV == AppEnvironment.production
    else logging.DEBUG
)
logging.basicConfig(level=logging_level)
logger = logging.getLogger("AppLogger")
