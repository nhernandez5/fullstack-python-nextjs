class ActionNotAllowedError(Exception):
    def __init__(self, message):
        super().__init__(message)


class AuthorizationRequiredError(Exception):
    def __init__(self, message):
        super().__init__(message)


class EntityNotFoundError(Exception):
    def __init__(self, message):
        super().__init__(message)


class InsufficientAccessError(Exception):
    def __init__(self, message):
        super().__init__(message)


class InvalidRequestError(Exception):
    def __init__(self, message):
        super().__init__(message)
