from enum import Enum


class AppEnvironment(Enum):
    development = "development"
    staging = "staging"
    production = "production"

    @staticmethod
    def from_str(text):
        if text == "DEVELOPMENT":
            return AppEnvironment.development

        elif text == "STAGING":
            return AppEnvironment.staging

        elif text == "PRODUCTION":
            return AppEnvironment.production

        else:
            raise NotImplementedError

    def __str__(self):
        return str(self.value).upper()
