import traceback

from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import Response

from app.core.errors import (
    ActionNotAllowedError,
    AuthorizationRequiredError,
    EntityNotFoundError,
    InvalidRequestError,
    InsufficientAccessError,
)


class CatchExceptionsMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        try:
            return await call_next(request)

        except AuthorizationRequiredError as ex:
            traceback.print_exc()
            return Response(
                "User is not authorized. Please login to continue.",
                status_code=401
            )

        except ActionNotAllowedError as ex:
            traceback.print_exc()
            return Response(str(ex), status_code=403)

        except EntityNotFoundError as ex:
            traceback.print_exc()
            return Response(str(ex), status_code=404)

        except InvalidRequestError as ex:
            traceback.print_exc()
            return Response(str(ex), status_code=400)

        except InsufficientAccessError as ex:
            traceback.print_exc()
            return Response(str(ex), status_code=403)

        except Exception:
            traceback.print_exc()
            return Response("Internal server error", status_code=500)


class SuppressNoResponseReturnedMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        try:
            return await call_next(request)

        except RuntimeError as ex:
            if str(ex) == 'No response returned.' and await request.is_disconnected():
                return Response(status_code=204)

            raise ex
