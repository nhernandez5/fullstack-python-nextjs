from fastapi import FastAPI

from app.core.app_environment import AppEnvironment
from app.core.config import settings
from app.core.logger import logger
from app.core.middleware import (
    CatchExceptionsMiddleware,
    SuppressNoResponseReturnedMiddleware,
)
from app.db.session import (
    close_db_connection_pools,
    open_db_connection_pools,
)
from app.routes.v1.router import router as v1_router


app = FastAPI(
    openapi_url=f"{settings.API_PREFIX}/openapi.json",
    title="sample-api",
    version="0.1.1",
)


@app.on_event("startup")
def open_database_connection_pools():
    logger.info("Booting up api...")
    open_db_connection_pools()
    logger.info("Opened connection to DB...")


@app.on_event("shutdown")
async def close_database_connection_pools():
    logger.info("Shutting down api...")
    await close_db_connection_pools()
    logger.info("Closing connections to DB...")


app.add_middleware(SuppressNoResponseReturnedMiddleware)
app.add_middleware(CatchExceptionsMiddleware)


app.include_router(v1_router, prefix=f"{settings.API_PREFIX}/v1")
if settings.RUNTIME_ENV != AppEnvironment.production:
    from app.routes.test.router import router as tests_router

    app.include_router(tests_router, prefix=f"{settings.API_PREFIX}/test")
    