from typing import Optional, AsyncIterable

from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.engine import Engine as Database
from sqlalchemy.orm import close_all_sessions, sessionmaker

# make sure all SQL Alchemy models are imported (app.db.tables) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28
from app.core.config import settings
from app.db import tables  # noqa: F401

if settings.POSTGREST_SSL_CERT:
    import ssl

    sslctx = ssl.create_default_context(cafile=settings.POSTGREST_SSL_CERT)
    sslctx.check_hostname = False

    _conn_args = {"ssl": sslctx}

else:
    _conn_args = {}

_db_conn: Optional[Database]


def open_db_connection_pools():
    global _db_conn
    global _db_session
    _db_conn = create_async_engine(
        settings.SQLALCHEMY_DATABASE_URI,
        connect_args=_conn_args,
        max_overflow=10,
        pool_pre_ping=True,
        pool_recycle=3600,
        pool_size=10,
        pool_timeout=30,
    )
    _db_session = sessionmaker(
        _db_conn,
        class_=AsyncSession,
        autoflush=False,
        expire_on_commit=False,
    )


async def close_db_connection_pools():
    global _db_conn
    if _db_conn:
        close_all_sessions()
        await _db_conn.dispose()


async def get_db_session() -> AsyncIterable[AsyncSession]:
    assert _db_session is not None
    async with _db_session() as session:
        try:
            yield session

        except Exception as ex:
            await session.rollback()

            raise ex

        finally:
            await session.close()
