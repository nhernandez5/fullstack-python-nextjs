from typing import Any, Optional

from pydantic import BaseModel


class AuthToken(BaseModel):
    access_token: str
    token_type: str


class Msg(BaseModel):
    msg: str
    data: Optional[Any] = None


class RefreshToken(AuthToken):
    refresh: Optional[str] = None


class TokenPayload(BaseModel):
    sub: Optional[Any] = None
